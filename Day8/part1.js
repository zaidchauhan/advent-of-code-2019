// imports
const fs  = require('fs');
const path = require('path');

//
const inputValues = fs.readFileSync(path.join(__dirname, '/input.txt'), 'utf8').toString()
                    .split('').map(item => parseInt(item, 10));
                    
//
const layers = [];

while (inputValues.length > 0) {
  const layer = [];
  for(let i=0; i<6; i++) {
    const row = [];
    for (let j = 0; j < 25; j++) {
      row.push(inputValues.shift());
    }
    layer.push(row);
  }
  layers.push(layer); 
}

// 
let lowestDigits, result;
layers.forEach((layer) => {
  const signleResult = layer.reduce((acc, value) => {
    acc[0] = (acc[0] + value.filter(item => item === 0).length);
    acc[1] = (acc[1] + value.filter(item => item === 1).length);
    acc[2] = (acc[2] + value.filter(item => item === 2).length);
    return acc;
  }, {'0': 0, '1': 0, '2': 0});

  if (lowestDigits === undefined || signleResult[0] < lowestDigits){
    lowestDigits = signleResult[0];
    result = signleResult[1] * signleResult[2];
  }
});

console.log('Result: ',result);
