// imports
const fs  = require('fs');
const path = require('path');

//
const inputValues = fs.readFileSync(path.join(__dirname, '/input.txt'), 'utf8').toString()
                    .split('').map(item => parseInt(item, 10));
                    
//
const matrixJ = 25, matrixI = 6;
const layers = [];

while (inputValues.length > 0) {
  const layer = [];
  for(let i=0; i<matrixI; i++) {
    const row = [];
    for (let j = 0; j < matrixJ; j++) {
      row.push(inputValues.shift());
    }
    layer.push(row);
  }
  layers.push(layer); 
}

//
const finalLayer = [];
for (let i = 0; i < matrixI; i++) {
  const row = [];
  for (let j = 0; j < matrixJ; j++) {
    let highest = 100;
    layers.some((layer) => {
      if(layer[i][j] < highest) {
        highest = layer[i][j];
        if(highest == 0 || highest == 1)
          return true;
      }
    });
    row.push(highest);
  }
  finalLayer.push(row);
}

// console.log('finalLayer: ', finalLayer);

//
let mapping = {
  0: ' ',
  1: '#'
}
let resultText = '';
finalLayer.forEach((rows) => {
  rows.forEach((item) => {
    resultText += mapping[item];
  });
  resultText += '\n';
});

console.log(resultText);