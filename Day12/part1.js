let fs = require('fs');
let path = require('path');

let position = [];
let velocity = [
  {x:0, y:0, z:0},
  {x:0, y:0, z:0},
  {x:0, y:0, z:0},
  {x:0, y:0, z:0}
];

fs.readFileSync(path.join(__dirname, '/input.txt'), 'utf8').trim()
.split('\n').map((line) => {
  const moonPosition = {};
  
  line = line.trim().substring(1, line.length-1);
  line.split(',').forEach(axis => {
    axis = axis.trim().split('=');
    moonPosition[axis[0]] = parseInt(axis[1]);
  });

  position.push(moonPosition);
});

//
for (let i=0; i<1000; i++) {
  //
  for (let j=0; j<4; j++) {
    //
    for (let k=j+1; k<4; k++) {
      //
      velocity[j].x += calculateGravity(position[j].x, position[k].x);
      velocity[j].y += calculateGravity(position[j].y, position[k].y);
      velocity[j].z += calculateGravity(position[j].z, position[k].z);
      //
      velocity[k].x += calculateGravity(position[k].x, position[j].x);
      velocity[k].y += calculateGravity(position[k].y, position[j].y);
      velocity[k].z += calculateGravity(position[k].z, position[j].z);
    }
  }

  position = position.map((moonPosition, index) => {
    moonPosition.x += velocity[index].x;
    moonPosition.y += velocity[index].y;
    moonPosition.z += velocity[index].z;

    return moonPosition;
  });  

  //console.log('position: ', position);
  //console.log('velocity: ', velocity);
}

//console.log('position: ', position);
const total = Object.keys(position).reduce((total, index) => {
  const pot = Object.values(position[index]).reduce((total, value) => total+Math.abs(value), 0);
  const kin = Object.values(velocity[index]).reduce((total, value) => total+Math.abs(value), 0);

  return total += pot * kin;
}, 0);

console.log('Total: ',total);

function calculateGravity(a, b) {
  return (a == b) ? 0 : (a > b ? -1 : 1)
}
