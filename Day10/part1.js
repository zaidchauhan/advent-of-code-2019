var fs = require('fs');
var path = require('path');

var matrix = fs.readFileSync(path.join(__dirname, '/input.txt'), 'utf8').trim()
.split('\n')
.map((line) => {
  return line.split('');
});

const rowsLength = matrix.length;
const columnsLength = matrix[0].length;

console.log(rowsLength, columnsLength);
let highest = 0;
let highestCordinates;

for(let outerRow=0; outerRow<rowsLength; outerRow++) {
  for(let outerColumn=0; outerColumn<columnsLength; outerColumn++) {
    //
    if(matrix[outerRow][outerColumn] !== '#')
      continue;
    //
    const visited = new Set();
    for(let innerRow=0; innerRow<rowsLength; innerRow++) {
      for(let innerColumn=0; innerColumn<columnsLength; innerColumn++) {
        //
        if(matrix[innerRow][innerColumn] == '#' && (innerRow!==outerRow || innerColumn!==outerColumn)){
          const diffRow = innerRow - outerRow;
          const diffColumn = innerColumn - outerColumn;

          visited.add(Math.atan2(diffColumn, diffRow));
        }
      }
    }

    if(visited.size > highest) {
      highest = visited.size;
      highestCordinates = {row: outerRow, column: outerColumn};
    }
  }
}

console.log('highest: ', highest);
console.log('highestCordinates: ', highestCordinates); //Row is - Y axis and Column is X axis