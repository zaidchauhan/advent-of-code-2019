// //
var fs = require('fs');
var path = require('path');

const monitoringStation = {row: 36, column: 26}; // Row = Y Axis, Column = X Axis
var matrix = fs.readFileSync(path.join(__dirname, '/input.txt'), 'utf8').trim()
.split('\n')
.map((line => line.split('')));

//
const rowsLength = matrix.length;
const columnsLength = matrix[0].length;

let aestorids =[];

for(let row=0; row<rowsLength; row++) {
  for(let column=0; column<columnsLength; column++) {
    if(matrix[row][column] === '#' && !(monitoringStation.row === row && monitoringStation.column === column)) {
      aestorids.push(
        {
          row,
          column,
          angle: Math.atan2(row-monitoringStation.row, column-monitoringStation.column) * 180 / Math.PI,
          distance: Math.hypot(monitoringStation.row-row, monitoringStation.column-column)
        }
      );
    }
  }
}

//
aestorids.sort((a,b) => a.angle - b.angle);

const listOfAngles = [...new Set(aestorids.map(({ angle }) => angle))];
let currentAngle = listOfAngles.findIndex(angle => angle === -90);

//
let counter = 0;
let result;
while (aestorids.length) {
  const aestorid = aestorids.filter(({angle}) => angle === listOfAngles[currentAngle])
  .sort((a, b) => a.distance - b.distance)[0];

  if(aestorid) {
    aestorids = aestorids.filter(({row, column}) => row !== aestorid.row || column !== aestorid.column);

    console.log('counter: ', counter, ' row: ', aestorid.row, ' column: ',aestorid.column);
    if(counter === 199) {
      result = {row: aestorid.row, column: aestorid.column};
      break;
    }
    counter++;
  }

  currentAngle = currentAngle < listOfAngles.length ? currentAngle + 1 : 0;
}

console.log('result: ',result);
