// imports
const fs  = require('fs');
const path = require('path');
const compute = require('./intcode-solved')

async function solveForSecondStar (instructions, phases) {
  phases = phases || permutations([5,6,7,8,9]);
  console.log('Phase combination count', phases.length)

  const { maxSignal, phaseSettings } = await computeMaxFeedbackSignal(instructions, phases)
  console.log('Solution 2', 'used phase setting', phaseSettings, 'Answer:', maxSignal)
}

async function computeMaxFeedbackSignal (instructions, phases) {
  let maxSignal = 0
  let maxPhaseSettings = []
  let phaseSettings

  function outputE (signal) {
    if (signal > maxSignal) {
      console.log(phaseSettings, 'New max found', signal, '>', maxSignal)
      maxSignal = signal
      maxPhaseSettings = phaseSettings
    }
  }

  while (phases.length > 0) {
    phaseSettings = phases.pop()

    const a = {
      phase: phaseSettings[0],
      memory: instructions,
      outputs: []
    }
    const b = {
      phase: phaseSettings[1],
      memory: instructions,
      outputs: []
    }
    const c = {
      phase: phaseSettings[2],
      memory: instructions,
      outputs: []
    }
    const d = {
      phase: phaseSettings[3],
      memory: instructions,
      outputs: []
    }
    const e = {
      phase: phaseSettings[4],
      memory: instructions,
      outputs: []
    }

    a.inputs = e.outputs
    b.inputs = a.outputs
    c.inputs = b.outputs
    d.inputs = c.outputs
    e.inputs = d.outputs

    e.outputs.push(a.phase, 0)
    a.outputs.push(b.phase)
    b.outputs.push(c.phase)
    c.outputs.push(d.phase)
    d.outputs.push(e.phase)

    const amplifiers = [
      compute(instructions, a.inputs, a.outputs),
      compute(instructions, b.inputs, b.outputs),
      compute(instructions, c.inputs, c.outputs),
      compute(instructions, d.inputs, d.outputs),
      compute(instructions, e.inputs, e.outputs, outputE, 'E')
    ]

    await Promise.all(amplifiers)
  }

  return { maxSignal, phaseSettings: maxPhaseSettings }
}

async function run () {
  const input = fs.readFileSync(path.join(__dirname, '/input.txt'), 'utf8').toString()
										//.split(',').map(item => parseInt(item, 10));
  await solveForSecondStar(input);
}

function permutations(list)
{
	// Empty list has one permutation
	if (list.length == 0)
		return [[]];
		
		
	var result = [];
	
	for (var i=0; i<list.length; i++)
	{
		// Clone list (kind of)
		var copy = list.slice(0);

		// Cut one element from list
		var head = copy.splice(i, 1);
		
		// Permute rest of list
		var rest = permutations(copy);
		
		// Add head to each permutation of rest of list
		for (var j=0; j<rest.length; j++)
		{
			var next = head.concat(rest[j]);
			result.push(next);
		}
	}
	
	return result;
}

run();
