function runIntCode(instructions, inputs, outputs, outputCallback) {
  // let winner = 0;

  let calculate = (currentPosition, inputs, outputs, outputCallback) => {
    let value = instructions[currentPosition]; //.toString().split('');
    let rightValue = value.toString().length > 2 ? value.toString().substring(value.toString().length - 2, value.toString().length) : value.toString();
    
    let modes = value.toString().padStart(rightValue.length+3, '0');
    modes = modes.substring(0, modes.length-rightValue.length).split('');

    const opcode = value > 99 ? value % 100 : value;
    // const copyInstruction = [...instructions];
    // const opcode = Number.parseInt([value.pop(), value.pop()].reverse().join(''))
    // const modes = [Number.parseInt(value.pop() || 0), Number.parseInt(value.pop() || 0), Number.parseInt(value.pop() || 0)];

    const first = modes[2] == 1 ? instructions[currentPosition+1] : instructions[instructions[currentPosition+1]];
    const second = modes[1] == 1 ? instructions[currentPosition+2] : instructions[instructions[currentPosition+2]];

    switch (opcode) {    
      case 99:
        currentPosition = undefined;
        break;
      case 1:
        instructions[instructions[currentPosition+3]] = first + second;
        currentPosition += 4;
        break;
      case 2:
        instructions[instructions[currentPosition+3]] = first * second;
        currentPosition += 4;
        break;
      case 3:
        const input = inputs.shift();
        if(input !== undefined) {
          instructions[instructions[currentPosition+1]] = input;
          currentPosition += 2;
        } else {
          console.log('Waiting for input', inputs, outputs)
        }
        break;
      case 4:
        outputs.push(first);
        outputCallback(first);
        // winner = first;
        currentPosition += 2;
        break;
      case 5:
        currentPosition = first > 0 ? second : currentPosition+3;
        break;
      case 6:
        currentPosition = first==0 ? second : currentPosition+3;
        break;
      case 7:
        instructions[instructions[currentPosition+3]] = first < second ? 1 : 0;
        currentPosition += 4;
        break;
      case 8:
        instructions[instructions[currentPosition+3]] = first==second ? 1 : 0;
        currentPosition += 4;
        break;
    }

    return currentPosition;
  }

  //
  // while(currentPosition != undefined) {
  //   calculate(inputValues[currentPosition]);
  // }

  let executeStep = () => {
    do {
      //
      position = newPosition;
      newPosition = calculate(position, inputs, outputs, outputCallback);

      if(newPosition == position) {
        setTimeout(executeStep, 0);
      } else if(newPosition === undefined) {
        programEnd();
        return;
      }
    } while (newPosition != position);
  };
  
  //
  let programEnd;
  const promise = new Promise((resolve, reject) => {
    programEnd = resolve;
  });

  let position = 0;
  let newPosition = 0;
  outputCallback = outputCallback || function() {};
  executeStep();

  return promise;
}

function getValue(number) {
  return Number.parseInt([value.pop(), value.pop()].reverse().join(''));
}

module.exports = runIntCode;
