// imports
const fs  = require('fs');
const path = require('path');
const { runIntCode } = require('./intCode');

//
const inputValues = fs.readFileSync(path.join(__dirname, '/input.txt'), 'utf8').toString()
										.split(',').map(item => parseInt(item, 10));

//
async function calculateMaxSignal(phases) {
	let maxOutput = 0;
	let currentPhase;

	function outputCallback(output) {
		if(output > maxOutput) {
			maxOutput = output;
		}
	}

	while (phases.length > 0) {
		currentPhase = phases.shift();

		const a = {outputs: []};
		const b = {outputs: []};
		const c = {outputs: []};
		const d = {outputs: []};
		const e = {outputs: []};
		
		// set references 
		a.inputs = e.outputs;
		b.inputs = a.outputs;
		c.inputs = b.outputs;
		d.inputs = c.outputs;
		e.inputs = d.outputs;

		// set output as input for next
		// a.output = b.input (phase 2) || e.output = a.input (phase 1 and 0)
		a.outputs.push(currentPhase[1]);
		b.outputs.push(currentPhase[2]);
		c.outputs.push(currentPhase[3]);
		d.outputs.push(currentPhase[4]);
		e.outputs.push(currentPhase[0], 0);

		const processes = [
			runIntCode(inputValues, a.inputs, a.outputs),
			runIntCode(inputValues, b.inputs, b.outputs),
			runIntCode(inputValues, c.inputs, c.outputs),
			runIntCode(inputValues, d.inputs, d.outputs),
			runIntCode(inputValues, e.inputs, e.outputs, outputCallback),
		];

		await Promise.all(processes);
	}
	return maxOutput;
}

// let highestOutput = 0;
const combinations = permutations([5,6,7,8,9]);

/* Example*/
(async () => {
	const result = await calculateMaxSignal([[9,8,7,6,5]]);
	console.log('result: ',result);
})();

// Utils
function permutations(list)
{
	// Empty list has one permutation
	if (list.length == 0)
		return [[]];
		
		
	var result = [];
	
	for (var i=0; i<list.length; i++)
	{
		// Clone list (kind of)
		var copy = list.slice(0);

		// Cut one element from list
		var head = copy.splice(i, 1);
		
		// Permute rest of list
		var rest = permutations(copy);
		
		// Add head to each permutation of rest of list
		for (var j=0; j<rest.length; j++)
		{
			var next = head.concat(rest[j]);
			result.push(next);
		}
	}
	
	return result;
}