//
let fs = require('fs');
let path = require('path');
let runIntCode = require('./intCode');

//
var instructions = fs.readFileSync(path.join(__dirname, '/input.txt'), 'utf8').trim()
.split(',').map(item => parseInt(item));


//
const movementMap = {
  '^': {x: 0, y: -1},
  '>': {x: 1, y: 0},
  'v': {x: 0, y: 1},
  '<': {x: -1, y: 0},
};

const directionMap = {
  '^0': '<',
  '^1': '>',
  'v0': '>',
  'v1': '<',
  '<0': 'v',
  '<1': '^',
  '>0': '^',
  '>1': 'v'
};

(async () => {
  const paintedPanels = {};
  const currentPosition = {x: 0, y: 0};
  const inputs = [];
  const outputs = [];

  let direction = '^';

  const inputCallback = (inputs) => {
    const existingColor = paintedPanels[`${currentPosition.x},${currentPosition.y}`];
    inputs.push(existingColor || 0);
  }

  const outputCallback = (outputs) => {
    if(outputs.length >= 2) {
      let color = outputs.shift();
      let turn = outputs.shift();

      // console.log(`key: ${currentPosition.x},${currentPosition.y}`);
      paintedPanels[`${currentPosition.x},${currentPosition.y}`] = color;
      direction = directionMap[direction+turn];

      const nextMovement = movementMap[direction];
      currentPosition.x += nextMovement.x;
      currentPosition.y += nextMovement.y;
    }
  }

  await runIntCode(instructions, inputs, inputCallback, outputs, outputCallback);

  console.log('Panels printed: ', Object.keys(paintedPanels).length);
})();