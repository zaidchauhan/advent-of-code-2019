function runIntCode(instructions, inputs, notifyWaitingForInput, outputs, publishOutput) {
  // let currentPosition=0;
  // let outputs = [];
  let relativeBase = 0;

  function getInstructionValue(mode, position) {
    switch (mode){
      case '0':
        return instructions[instructions[position]] || 0;
      case '1':
        return instructions[position];
      case '2':
        return instructions[instructions[position] + relativeBase] || 0;
    }
  }

  function getInstructionAddress(mode, position) {
    switch (mode) {
      case '2':
        return instructions[position] + relativeBase;
      default:
        return instructions[position];
    }
  }

  let calculate = (currentPosition, inputs, notifyWaitingForInput, outputs, publishOutput) => {
    let value = instructions[currentPosition];

    let rightValue = value.toString().length > 2 ? value.toString().substring(value.toString().length - 2, value.toString().length) : value.toString();
    let modes = value.toString().padStart(rightValue.length+3, '0');
    modes = modes.substring(0, modes.length-rightValue.length).split('');

    const opcode = value > 99 ? value % 100 : value;
    const first = getInstructionValue(modes[2], currentPosition+1);
    const second = getInstructionValue(modes[1], currentPosition+2);

    switch (opcode) {
      case 99:
        currentPosition = undefined;
        break;
      case 1:
        instructions[getInstructionAddress(modes[0], currentPosition+3)] = first + second;
        currentPosition += 4;
        break;
      case 2:
        instructions[getInstructionAddress(modes[0], currentPosition+3)] = first * second;
        currentPosition += 4;
        break;
      case 3:
        const input = inputs.shift();
        if(input !== undefined) {
          instructions[getInstructionAddress(modes[2], currentPosition+1)] = input;
          currentPosition += 2;
        } else {
          notifyWaitingForInput(inputs);
          //console.log('Waiting for input', inputs)
        }
        break;
      case 4:
        outputs.push(first);
        publishOutput(outputs);
        currentPosition += 2;
        break;
      case 5:
        currentPosition = first > 0 ? second : currentPosition+3;
        break;
      case 6:
        currentPosition = first==0 ? second : currentPosition+3;
        break;
      case 7:
        instructions[getInstructionAddress(modes[0], currentPosition+3)] = first < second ? 1 : 0;
        currentPosition += 4;
        break;
      case 8:
        instructions[getInstructionAddress(modes[0], currentPosition+3)] = first==second ? 1 : 0;
        currentPosition += 4;
        break;
      case 9:
        relativeBase += first;
        currentPosition += 2;
        break;
    }

    return currentPosition;
  }
  
  let executeStep = () => {
    do {
      //
      position = newPosition;
      newPosition = calculate(position, inputs, notifyWaitingForInput , outputs, publishOutput);

      if(newPosition == position) {
        setTimeout(executeStep, 0);
      } else if(newPosition === undefined) {
        programEnd();
        return;
      }
    } while (newPosition != position);
  };

  //
  let programEnd;
  const promise = new Promise((resolve, reject) => {
    programEnd = resolve;
  });

  let position = 0;
  let newPosition = 0;
  executeStep();

  return promise;

  // while(currentPosition != undefined && currentPosition != instructions.length) {
  //   calculate(instructions[currentPosition]);
  // }

  // return outputs.join(',');
}

module.exports = runIntCode;
