//
let fs = require('fs');
let path = require('path');
let runIntCode = require('./intCode');

//
var instructions = fs.readFileSync(path.join(__dirname, '/input.txt'), 'utf8').trim()
.split(',').map(item => parseInt(item));

// replace first point to play free
instructions[0] = 2;
console.log('instruction: ', instructions.length);

(async () => {

  let ballPosition = 0;
  let paddlePosition = 0;
  let bestScore = 0;

  let inputCallback = (inputs) => {
    if(paddlePosition < ballPosition) {
      inputs.push(1);
    } else if(paddlePosition > ballPosition) {
      inputs.push(-1);
    } else {
      inputs.push(0);
    }
  }

  let outputCallback = (outputs) => {
    if(outputs.length === 3) {
      const x = outputs.shift();
      const y = outputs.shift();
      const typePanel = outputs.shift();

      if (typePanel === 3) {
        paddlePosition = x; // x-axis
      } else if(typePanel === 4) {
        ballPosition = x;
      } else if(x === -1 && y === 0 && ![0,1,2,3,4].includes(typePanel)) {
        bestScore = typePanel;
      }
    }
  }

  await runIntCode(instructions, [], inputCallback, [], outputCallback);
  console.log('bestScore: ', bestScore);
})();