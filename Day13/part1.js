//
let fs = require('fs');
let path = require('path');
let runIntCode = require('./intCode');

//
var instructions = fs.readFileSync(path.join(__dirname, '/input.txt'), 'utf8').trim()
.split(',').map(item => parseInt(item));

console.log('instruction: ', instructions.length);

const outputs = [];

(async () => {

  await runIntCode(instructions, [], () => {}, outputs, () => {});
  console.log('outputs: ', outputs.length);

  let blockTiles = 0;
  for (let i = 2; i < outputs.length; i=i+3) {
    if(outputs[i] == 2) {
      blockTiles++;
    }
  }

  console.log('Block tiles: ', blockTiles);
})();