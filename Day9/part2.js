// imports
const fs  = require('fs');
const path = require('path');
const {runIntCode} = require('./intCode');

//
const instructions = fs.readFileSync(path.join(__dirname, '/input.txt'), 'utf8').toString()
										.split(',').map(item => parseInt(item, 10));
                                        
//
const inputs = [2];
const result = runIntCode(instructions, inputs);

console.log('result: ', result);